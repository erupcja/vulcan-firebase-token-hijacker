import fastify from "fastify";
import { encode } from "@wulkanowy/qr-node";
import bwipjs from "bwip-js";
import os from "os";

const port = parseInt(process.env.PORT || "3000", 10);

const app = fastify();

const localIP = (Object.values(os.networkInterfaces())
  .flat()
  .filter((iface) => !!iface) as (os.NetworkInterfaceInfoIPv4 | os.NetworkInterfaceInfoIPv6)[])
  .filter((iface) => !iface.internal)
  .filter((iface) => !/^(fe80|1(27|69)|::1)/.test(iface?.address))
  .map((iface) => iface.address)[0];

const done = (token: string, appName: string) => {
  console.log(`Firebase Token (for ${appName} API): ${token}`);
  app.close();
};

// png file with a QR code to be scanned by the official app
app.get("/qr.png", async (req, res) => {
  res.header("Content-Type", "image/png");
  res.send(
    // @ts-ignore invalid typedefs
    await bwipjs.toBuffer({
      bcid: "qrcode",
      paddingheight: 5,
      paddingwidth: 5,
      height: 100,
      width: 100,
      text: encode(
        "tDVS4ykCBBAeN33h",
        `CERT#http://${localIP}:${port}/erupcja/mobile-api#FK100000#ENDCERT`,
      ),
    }),
  );
});

// hijacker for legacy mobile-api apps (pl.vulcan.uonetmobile)
app.post("/erupcja/mobile-api/Uczen.v3.UczenStart/Certyfikat", (req, res) => {
  const body = req.body as {
    FirebaseTokenKey: string;
  }; // there are other values but they're unimportant
  res.header("Content-Type", "application/json");
  res.send(
    JSON.stringify({
      IsError: true,
      IsMessageForUser: true,
      Message: "Token hijacked",
      TokenKey: null,
    }),
  );
  done(body.FirebaseTokenKey, "legacy mobile");
});

// hijacker for hebe api apps (pl.edu.vulcan.hebe)
app.post("/erupcja/api/mobile/register/new", (req, res) => {
  const body = req.body as {
    FirebaseToken: string;
  }; // there are other values but they're unimportant
  res.header("Content-Type", "application/json");
  res.send(
    JSON.stringify({
      Status: {
        Code: 2137,
        Message: "Token hijacked",
      },
    }),
  );
  done(body.FirebaseToken, "hebe");
});

app.listen(
  {
    port,
    host: localIP,
  },
  (error) => {
    if (error) throw error;
    console.log(`Listening, now open http://${localIP}:${port}/qr.png`);
  },
);
