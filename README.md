# vulcan-token-hijacker

Sets up a local HTTP server and generates a QR code to trick official Vulcan apps (pl.edu.vulcan.hebe and pl.vulcan.uonetmobile) into sending the Firebase Cloud Messaging token to it.

This is an implementation made for your reference, feel free to port/modify it for your needs.

MRs are welcome.

How to run:
Requires Node.js, yarn, git.

```sh
$ git clone https://framagit.org/erupcja/vulcan-firebase-token-hijacker.git
$ cd vulcan-token-hijacker
$ yarn
$ yarn start
```

Also see: [erupcja/fcm-listener-node](https://framagit.org/erupcja/fcm-listener-node)
